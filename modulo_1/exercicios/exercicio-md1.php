<?php

class Produto
{
    private string $nome;
    private string $descricao;
    private float $preco;

    public function __construct(string $nomeProduto, string $descricaoProduto, float $precoProduto)
    {
        $this->nome = $nomeProduto;
        $this->descricao = $descricaoProduto;
        $this->preco = $precoProduto;
    }
}

$produto1 = new Produto("Paçoca", "Muito gostosa!", "1.50");
$produto2 = new Produto("Bandeclay", "Coisa de curitibano", "2.00");
var_dump($produto1);
var_dump($produto2);